//
//  FeedbackPresenter.swift
//  Feedback
//
//  Created by Lampros Zouloumis on 18/5/21.
//

import Foundation

public class FeedbackPresenter: FeedbackViewControllerToPresenterProtocol, FeedbackInteractorToPresenterProtocol{

    var router: FeedbackPresenterToRouterProtocol?
    var interactor: FeedbackPresenterToInteractorProtocol?
    var feedbackView: FeedbackPresenterToViewControllerProtocol?

}
