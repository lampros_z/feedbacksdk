//
//  FeedBackViewController.swift
//  Feedback
//
//  Created by Lampros Zouloumis on 18/5/21.
//

import Foundation
import UIKit
import MessageUI

class FeedbackViewController: UIViewController, FeedbackPresenterToViewControllerProtocol{
    var presenter: FeedbackViewControllerToPresenterProtocol?
    
    private var imageData: Data?
    
    lazy var container: UIView = {
        let container = UIView()
        return container
    }()
    
    lazy var header: UILabel = {
       let label = UILabel()
        label.text = "Το σχόλιό σας"
        label.font = UIFont(name: "FiraSans-Medium", size: 18)
        label.textColor = .white
        return label
    }()
    
    lazy var commmentTextArea: UITextView = {
       let textView = UITextView()
        textView.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae lectus tempor, iaculis mauris non, ultricies dolor."
        textView.font = UIFont(name: "FiraSans-Medium", size: 16)
        textView.textColor = .white
        textView.backgroundColor = UIColor(red: 27.0/255.0, green: 27.0/255.0, blue: 29.0/255.0, alpha: 1)
        return textView
    }()

    lazy var horizontalLine: UIView = {
       let line = UIView()
        line.backgroundColor = UIColor(red: 80.0/255.0, green: 162.0/255.0, blue: 53.0/255.0, alpha: 1)
        return line
    }()
    
    lazy var imageButton: UIButton = {
       let button = UIButton()
        button.setTitle("Προσθήκη φωτογραφίας", for: .normal)
        button.setBackgroundImage(UIImage(named: "btn_rounded_1", in: Bundle(for: FeedbackViewController.self), compatibleWith: nil), for: .normal)
        button.addTarget(self, action: #selector(selectImage), for: .touchUpInside)
        return button
    }()
    
    lazy var imagePicker = UIImagePickerController()
    
    lazy var submitButton: UIButton = {
        let button = UIButton()
        button.setTitle("Υποβολή", for: .normal)
        button.setTitleColor(UIColor(red: 133.0/255.0, green: 195.0/255.0, blue: 112.0/255.0, alpha: 1), for: .normal)
        button.setBackgroundImage(UIImage(named: "btn_rounded", in: Bundle(for: FeedbackViewController.self), compatibleWith: nil), for: .normal)
        button.addTarget(self, action: #selector(sendAction), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Feedback"
        navigationController?.navigationBar.backItem?.backButtonTitle = ""
        navigationController?.navigationBar.backItem?.title = UserDefaults.standard.string(forKey: "settingsTitle") ?? "Ρυθμίσεις"
        navigationController?.navigationBar.tintColor = .white
        view.backgroundColor = UIColor(red: 27.0/255.0, green: 27.0/255.0, blue: 29.0/255.0, alpha: 1)
        view.addSubview(imageButton)
        view.addSubview(submitButton)
        container.addSubview(header)
        container.addSubview(commmentTextArea)
        container.addSubview(horizontalLine)
        view.addSubview(container)
        setupConstraints()
    }
    
    func setupConstraints(){
        imageButton.translatesAutoresizingMaskIntoConstraints = false
        submitButton.translatesAutoresizingMaskIntoConstraints = false
        container.translatesAutoresizingMaskIntoConstraints = false
        header.translatesAutoresizingMaskIntoConstraints = false
        commmentTextArea.translatesAutoresizingMaskIntoConstraints = false
        horizontalLine.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 39),
            imageButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36),
            imageButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -229),
            submitButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 94),
            submitButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -93),
            submitButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -47),
            container.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 38),
            container.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -37),
            container.topAnchor.constraint(equalTo: view.topAnchor, constant: 136),
            container.heightAnchor.constraint(equalToConstant: 188),
            header.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 2),
            header.topAnchor.constraint(equalTo: container.topAnchor),
            commmentTextArea.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            commmentTextArea.trailingAnchor.constraint(equalTo: container.trailingAnchor),
            commmentTextArea.topAnchor.constraint(equalTo: header.bottomAnchor, constant: 7),
            commmentTextArea.heightAnchor.constraint(equalToConstant: 150),
            horizontalLine.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            horizontalLine.trailingAnchor.constraint(equalTo: container.trailingAnchor),
            horizontalLine.bottomAnchor.constraint(equalTo: container.bottomAnchor),
            horizontalLine.heightAnchor.constraint(equalToConstant: 2)
        ])
    }
    
    
    
    @objc func selectImage(sender: UIButton!){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){

            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false

            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @objc func sendAction(sender: UIButton!){
        if MFMailComposeViewController.canSendMail() {
          let mail = MFMailComposeViewController()
          mail.mailComposeDelegate = self
          mail.setCcRecipients(["lzouloumis@threenitas.com"])
          mail.setSubject("Feedback")
          mail.setMessageBody(commmentTextArea.text, isHTML: false)
          if let validImage = imageData{
              mail.addAttachmentData(validImage, mimeType: "image/png", fileName: "screenshot.png")
          }
          self.present(mail, animated: true, completion: nil)
        }
    }
    
}

extension FeedbackViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
         })
        let imageKey = UIImagePickerController.InfoKey.originalImage
        let image = info[imageKey] as! UIImage
        if let imageData = image.pngData(){
            self.imageData = imageData
        }
    }
}

extension FeedbackViewController: MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
