//
//  FeedbackProtocol.swift
//  Feedback
//
//  Created by Lampros Zouloumis on 18/5/21.
//

import Foundation
import UIKit

protocol FeedbackViewControllerToPresenterProtocol: AnyObject{
    var router: FeedbackPresenterToRouterProtocol? {get set}
    var interactor: FeedbackPresenterToInteractorProtocol? {get set}
    var feedbackView: FeedbackPresenterToViewControllerProtocol?{get set}
    //Comment
}

protocol FeedbackPresenterToViewControllerProtocol: AnyObject{
    var presenter: FeedbackViewControllerToPresenterProtocol? {get set}
    
}

protocol FeedbackPresenterToRouterProtocol: AnyObject{

    static func createFeedbackModule() -> UIViewController
    
}

protocol FeedbackPresenterToInteractorProtocol: AnyObject {
    var presenter: FeedbackInteractorToPresenterProtocol? {get set}
    
}

protocol FeedbackInteractorToPresenterProtocol: AnyObject {
    
}
