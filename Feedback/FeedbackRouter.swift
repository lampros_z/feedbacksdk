//
//  FeedbackRouter.swift
//  Feedback
//
//  Created by Lampros Zouloumis on 18/5/21.
//

import Foundation
import UIKit

typealias FeedbackPresenterType = FeedbackPresenter & FeedbackViewControllerToPresenterProtocol & FeedbackInteractorToPresenterProtocol
typealias FeedbackInteractorType = FeedbackInteractor & FeedbackPresenterToInteractorProtocol
typealias FeedbackViewControllerType = FeedbackPresenterToViewControllerProtocol & UIViewController

public class FeedbackRouter: FeedbackPresenterToRouterProtocol{
    public static func createFeedbackModule() -> UIViewController {
        let feedbackRouter: FeedbackRouter = FeedbackRouter()
        
        let feedbackPresenter: FeedbackPresenterType = FeedbackPresenter()
        let feedbackViewController: FeedbackViewControllerType = FeedbackViewController()
        let feedbackInteractor: FeedbackInteractorType = FeedbackInteractor()
        
        feedbackViewController.presenter = feedbackPresenter
        
        feedbackPresenter.feedbackView = feedbackViewController
        feedbackPresenter.interactor = feedbackInteractor
        feedbackPresenter.router = feedbackRouter
        
        feedbackInteractor.presenter = feedbackPresenter
        return feedbackViewController
    }
    
    
}
